exports.handler = async (event) => {
    const min = 1;
    const max = 10;    
    const randomNumber = Math.floor(
      Math.random() * (max - min + 1)
    ) + min;
    const message = 'Nomer keberuntungan kamu adalah: ' +
      randomNumber;
    return message;
  };
